/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author gabriel
 */
public class Circulo extends Elipse{
    private double rc;

    /**
     *
     */
    public Circulo() {
        super(0x00, 0x00);
//        super(semi_eixo_x, semi_eixo_y);
    }
    
    public Circulo(double raio) {
        super(0x00, 0x00);
        this.rc = raio;
    }
    
    /**
     *
     * @return
     */
    @Override
    public double getPerimetro(){
        return Math.PI * 2 * rc;
    }
}
